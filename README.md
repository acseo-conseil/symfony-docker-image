# Symfony Docker Image

Image Docker de base pour projet Symfony d'Acseo (nginx, php, php-fpm, composer, node, yarn, wkhtmltopdf, ...)

## Images disponible

[Container registry](https://gitlab.acseo.co/acseoteam/symfony-docker-image/container_registry/eyJuYW1lIjoiYWNzZW90ZWFtL3N5bWZvbnktZG9ja2VyLWltYWdlL3N5bWZvbnkiLCJ0YWdzX3BhdGgiOiIvYWNzZW90ZWFtL3N5bWZvbnktZG9ja2VyLWltYWdlL3JlZ2lzdHJ5L3JlcG9zaXRvcnkvMjA3L3RhZ3M%2FZm9ybWF0PWpzb24iLCJpZCI6MjA3fQ==
)

## Example d'utilisation 

acseoteam/symfony-stack>

## Build

Les builds sont automatiquement lancés sur les branches ou tags nommés `phpX.Y` ou `phpX.Y.Z` (leurs créations est protégés et n'est possible que pour les mainteneurs de ce dépôt).

## Comment créer une nouvelle version patch (par exemple php7.4.12)

1. Tirer une nouvelle branche depuis `php7.4` et la nommée `feature/php7.4.12`
2. Une fois les développements finis, créer une MR sur la branche `php7.4`
3. Lorsque la MR est validée par au moins 2 personnes, la merger.
4. Créer un nouveau tag `php7.4.12` depuis la branche `php7.4` 

## Comment créer une nouvelle version mineure (par exemple php7.4)

Il vous suffit de créer une nouvelle branche nommée `php7.4`
