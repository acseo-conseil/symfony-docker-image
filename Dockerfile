FROM        php:7.4.13-fpm
LABEL       maintainer="ACSEO <contact@acseo.fr>"

#
# Here, we install system dependencies that will be needed to run configuration
# or PHP extensions installations.
#

# Add Debian jessie sources as libpng12-0 libssl1.0.0 xfonts-75dpi fontconfig packages are missing in Debian stretch
RUN         echo "deb http://ftp.de.debian.org/debian jessie main" >> /etc/apt/sources.list \
            && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
            && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
            && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN         apt-get -qq update --fix-missing \
            && apt-get -qq install --no-install-recommends -y \
                build-essential \
                curl \
                cron \
                fontconfig \
                git \
                libfontconfig \
                libfreetype6-dev \
                libicu-dev \
                libjpeg62-turbo-dev \
                libonig-dev \
                libpng-dev \
                libpng12-0 \
                libssl1.0.0 \
                libwebp-dev \
                libxml2-dev \
                libxpm-dev \
                libxrender-dev \
                libzip-dev \
                nginx \
                nodejs \
                software-properties-common \
                unzip \
                vim \
                wget \
                xfonts-75dpi \
                xfonts-base \
                xvfb \
                yarn \
                zip \
                apache2-utils \
            && rm -rf /var/lib/apt/lists/*

#
# Install wkhtmltopdf (manually as the package in the Debian repository does not support option --header-html)
#
RUN         wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.jessie_amd64.deb \
            && dpkg -i wkhtmltox_0.12.5-1.jessie_amd64.deb \
            && rm -rf wkhtmltox_0.12.5-1.jessie_amd64.deb

#
# We install & configure needed PHP extensions.
#
RUN         docker-php-ext-configure gd --with-freetype --with-jpeg \
            && docker-php-ext-install -j "$(nproc)" gd

RUN         docker-php-ext-install \
                iconv \
                intl \
                mbstring \
                mysqli \
                opcache \
                pdo \
                pdo_mysql \
                xml \
                zip \
                exif

RUN         pecl install \
                apcu \
            && docker-php-ext-enable \
                apcu \
                opcache

#
# Set timezone
#
RUN ln -snf /usr/share/zoneinfo/Europe/Paris /etc/localtime && echo Europe/Paris > /etc/timezone

#
# Then, we copy & run Composer installation script.
#
ENV         COMPOSER_HOME=/var/composer
COPY        ./composer-install /tmp/composer-install
RUN         chmod +x /tmp/composer-install \
            && /tmp/composer-install \
            && rm /tmp/composer-install \
            && mkdir -p /var/composer \
            && chown -R www-data:www-data /var/composer

#
# Yarn config
#
RUN         mkdir -p /var/www/.yarn \
            && mkdir -p /var/www/.cache \
            && touch /var/www/.yarnrc \
            && chown -R www-data:www-data /var/www
#
# php, php-fpm and nginx config
#
COPY        ./php.conf.d/* /usr/local/etc/php/conf.d/
COPY        ./php-fpm.d/* /usr/local/etc/php-fpm.d/
COPY        ./nginx.conf.d/* /etc/nginx/conf.d/
RUN         rm /etc/nginx/sites-enabled/default

COPY        ./app-start /usr/local/bin/app-start
RUN         chmod +x /usr/local/bin/app-start

CMD         ["app-start"]
